// Hier werden die Experimente durchgeführtmit den verschiedenen Parameter
// Aktuell sind die Einstellungen für Simulated-Annealing vorgenommen  

public class Experimente {
    public static void main(String[] args) {
        int gridLength = Integer.parseInt(args[0]);
        Spielfeld spielfeld = new Spielfeld(gridLength);
        double temp = 0;
        double rate = 0;
        switch (args[1]) {
            case "Klein" -> {
                temp = 100;
                System.out.println("Klein");
            }
            case "Mittel" -> {
                temp = 1000;
                System.out.println("Mittel");
            }
            case "Gross" -> {
                temp = 10000;
                System.out.println("Gross");
            }
        }

        switch (args[2]) {
            case "Klein" -> {
                rate = 0.001;
                System.out.println("Klein");
            }
            case "Mittel" -> {
                rate = 0.01;
                System.out.println("Mittel");
            }
            case "Gross" -> {
                rate = 0.1;
                System.out.println("Gross");
            }
        }
        System.out.println(temp + " " + rate);
        var sA = new SimulatedAnnealing(spielfeld, gridLength, temp, rate);
        Spielfeld bestGrid;
        int correct = 0;
        int incorrect = 0;
        double iterations = 1000;
        final long timeStartBegin = System.currentTimeMillis();
        for (int i = 0; i < iterations; i++) {
            System.out.println("Iteration: [" + (i+1) + "/" + (int) iterations + "]");
            final long timeStart = System.currentTimeMillis();
            sA.run();
            final long timeEnd = System.currentTimeMillis();
            bestGrid = sA.getBestGrid();
            bestGrid.printGrid();
            if (Utility.unvalidQueensSum(bestGrid)== 0) {
                correct++;
                System.out.println("Dauer: " + ((double)(timeEnd - timeStart)/1000)+ "s\n");
            } else {
                incorrect++;
                System.out.println("Das Ergebnis ist nicht optimal :(");
                System.out.println("Dauer: " + ((double)(timeEnd - timeStart)/1000) + "s\n");
            }

            spielfeld.reset(gridLength);
            sA.reset();
        }
        final long timeEndEnd = System.currentTimeMillis();
        double timeSumAll = (timeEndEnd - timeStartBegin) / 1000.0;
        System.out.println("Gesamtzeit: " + timeSumAll);
        System.out.println(" " + gridLength);
        System.out.println("Bis zum Finden einer Lösung hat es durchschnittlich " + (timeSumAll/correct) + "s gedauert");
        System.out.println("Richtig: " + correct);
        System.out.println("Falsch: " + incorrect);
        double fehlerrate = ((incorrect*100) / (iterations));
        System.out.println("Fehlerrate: " + fehlerrate + "%");
    }
}
