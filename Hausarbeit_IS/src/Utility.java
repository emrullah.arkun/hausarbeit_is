public final class Utility {

    /**
     * Berechnnung aller ungültigen Nachbarn aller Damen
     * @param spielfeld Spielfeld, wo sich die Damen befinden
     * @return s.o.
     */
    public static int unvalidQueensSum(Spielfeld spielfeld){
        int c = 0;
        for (int[] queen: spielfeld.getListOfQueens()){
            c += countUnvalidQueens(queen,spielfeld);
        }
        return c;
    }

    /**
     * Berechnung aller ungültigen Nachbarn einer Dame
     * @param actualQueenPosition Aktuelle Position einer Dame, actualQueenPosition[0] = x, actualQueenPosition[1] = y
     * @param spielfeld Das Spielfeld, wo die Dame sich befindet
     * @return s.o.
     */
    public static int countUnvalidQueens(int[] actualQueenPosition, Spielfeld spielfeld){
        // Zufällig eine Dame bestimmen
        int x = actualQueenPosition[0];
        int y = actualQueenPosition[1];
        int zahlUngueltigerNachbarn = 0;
        int gridLen = spielfeld.getGrid().length;
        // Zeile checken
        for (int i = 0; i < gridLen; i++){
            if (spielfeld.getGrid()[x][i] == 1 && i != y) zahlUngueltigerNachbarn++;
        }
        // Spalte checken
        for (int i = 0; i < gridLen; i++){
            if (spielfeld.getGrid()[i][y] == 1 && i != x) zahlUngueltigerNachbarn++;
        }
        // Diagonale checken. Wenn Differenz Betrag gleich ist, ist es auf der Diagonalen
        for (int i = 0; i < spielfeld.getListOfQueens().length; i++){
            // Durch alle außer der Dame selber iterieren
            if (spielfeld.getListOfQueens()[i][0] != x && spielfeld.getListOfQueens()[i][1] != y){
                int[] dame = spielfeld.getListOfQueens()[i];
                int diffX = Math.abs(dame[0] - x);
                int diffY = Math.abs(dame[1] - y);
                if (diffX == diffY) zahlUngueltigerNachbarn++;
            }
        }
        return zahlUngueltigerNachbarn;
    }

    /**
     * Erzeugt eine Kopie eines Spielfeldes
     * @param spielfeld Spielfeld
     * @return Neues kopiertes Spielfeld
     */
    public static Spielfeld deepCopy(Spielfeld spielfeld) {
        if (spielfeld == null) {
            return null;
        }
        int[][] grid = spielfeld.getGrid();
        int[][] listOfQueens = spielfeld.getListOfQueens();
        int[][] newGrid = new int[grid.length][];
        int[][] newlistOfQueens = new int[listOfQueens.length][];
        for (int i = 0; i < grid.length; i++) {
            newGrid[i] = grid[i].clone();
        }
        for (int i = 0; i < listOfQueens.length; i++) {
            newlistOfQueens[i] = listOfQueens[i].clone();
        }
        return new Spielfeld(newGrid,newlistOfQueens);
    }
}
