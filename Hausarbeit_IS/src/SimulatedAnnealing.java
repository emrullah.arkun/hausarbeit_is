import java.util.Random;
public class SimulatedAnnealing{

    private int gridSize;
    // Aktuelle Temperatur
    private double TEMPERATURE;

    private final double resetTemp;
    // Das wird immer neu gesetzt, wenn die neuen Aufstellungen der Damen besser ist als das vorherige
    static Spielfeld bestGrid;
    // Um so viel sinkt die Temperatur bei jeder Iteration
    private double COOLING_RATE;
    // Spielfeld
    private static Spielfeld actualGrid;

    public SimulatedAnnealing(Spielfeld grid, int gridSize, double temperature, double cooling_rate){
        actualGrid = grid;
        this.gridSize = gridSize;
        this.TEMPERATURE = temperature;
        this.resetTemp = temperature;
        this.COOLING_RATE = cooling_rate;
    }

    public void reset(){
        TEMPERATURE = resetTemp;
        actualGrid.reset(gridSize);
        bestGrid.reset(gridSize);
    }

    // Algorithmus starten
    public void run(){
        Random rand = new Random();
        // random Position für alle Damen bestimmen
        actualGrid.randomPositions();
        // xBest = x
        bestGrid = Utility.deepCopy(actualGrid);
        Spielfeld newGrid;
        while (TEMPERATURE >= 0) {
            int[][] listOfQueens = actualGrid.getListOfQueens();
            // Zufällige Dame auswählen, indem Index zufällig generiert wird
            int randomQueen = rand.nextInt(listOfQueens.length);
            // Zufällige Dame auf dem Spielfeld speichern
            int[] actualRandomQueenPosition = listOfQueens[randomQueen];
            // Neue zufällige Position der gewählten Dame
            int[] newRandomQueenPosition = actualGrid.placeRandomPosition();

            // Spielfeld klonen, um vorherige Kosten mit nachherigen zu vergleichen (newGrid dient nur als Vergleichsspielfeld)
            newGrid = Utility.deepCopy(actualGrid);
            // Alte Dame auf dem Spielfeld löschen
            newGrid.deleteQueenAtIndex(actualRandomQueenPosition[0],actualRandomQueenPosition[1]);
            // Neue Dame setzen
            newGrid.setQueenAtIndex(newRandomQueenPosition[0],newRandomQueenPosition[1]);
            // Neue Queen in neuer Liste mit den anderen Damen speichern
            newGrid.changeQueensIndex(randomQueen, newRandomQueenPosition[0],newRandomQueenPosition[1]);

            // Summe aller ungültigen Damen auf dem Weg davor und danach
            // Wenn deltaE < 0 -> Neue Lösung besser
            // Wenn deltaE >= 0 -> alte Lösung besser
            int deltaE = Utility.unvalidQueensSum(newGrid) - Utility.unvalidQueensSum(actualGrid);
            // Wenn neue Kosten besser, dann diesen nehmen und listOfIndex aktualisieren, sonst neue Position mit Wahrscheinlichkeit nehmen
            if (deltaE <= 0) {
                // Alte Dame in der Liste ersetzen
                listOfQueens[randomQueen] = newRandomQueenPosition;
                actualGrid.setQueenAtIndex(newRandomQueenPosition[0], newRandomQueenPosition[1]);
                actualGrid.deleteQueenAtIndex(actualRandomQueenPosition[0], actualRandomQueenPosition[1]);
            } else {
                // Neue Position mit Wahrscheinlichkeit nehmen
                if (rand.nextDouble() < formula(deltaE)){
                    // Alte Dame in der Liste ersetzen
                    listOfQueens[randomQueen] = newRandomQueenPosition;
                    actualGrid.setQueenAtIndex(newRandomQueenPosition[0], newRandomQueenPosition[1]);
                    actualGrid.deleteQueenAtIndex(actualRandomQueenPosition[0], actualRandomQueenPosition[1]);
                }
            }

            // Wenn Kosten des neuen Spielfeldes besser sind -> xBest = x
            if (Utility.unvalidQueensSum(actualGrid) < Utility.unvalidQueensSum(bestGrid)){
                bestGrid = Utility.deepCopy(actualGrid);
            }
            if (Utility.unvalidQueensSum(bestGrid) == 0){
                System.out.println("Temperatur betrug: " + TEMPERATURE);
                break;
            }
            // Es wird kühler
            TEMPERATURE -= COOLING_RATE;
        }
        // Algorithmus beendet
    }

    private double formula(int deltaE) {
        return Math.exp(-deltaE / TEMPERATURE);
    }



    public Spielfeld getBestGrid() {
        return bestGrid;
    }
}
