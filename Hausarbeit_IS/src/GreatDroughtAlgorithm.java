import java.security.SecureRandom;

// Parameter
public class GreatDroughtAlgorithm {
    private int gridSize;
    // Initialer Wasserstand
    private double waterLevel;
    private double resetWaterLevel;
    // Um so viel steigt das Wasser
    private double growthRate;
    // Bestes Spielfeld
    static Spielfeld bestGrid;
    // Aktuelles Spielfeld
    private Spielfeld actualGrid;

    public void reset(){
        waterLevel = resetWaterLevel;
        actualGrid.reset(gridSize);
        bestGrid.reset(gridSize);
    }

    public GreatDroughtAlgorithm(Spielfeld grid, int gridSize, double waterlvl, double growthrate){
        this.actualGrid = grid;
        this.gridSize = gridSize;
        waterLevel = waterlvl;
        resetWaterLevel = waterLevel;
        growthRate = growthrate;
    }

    public void run(){
        SecureRandom rand = new SecureRandom();
        // random Position für alle Damen bestimmen
        actualGrid.randomPositions();
        // xBest = x
        bestGrid = Utility.deepCopy(actualGrid);
        Spielfeld newGrid;
        int previousSum = -1000;
        int counter = 0;
        int iteration = 0;
        while (Utility.unvalidQueensSum(bestGrid) != 0){
            iteration++;
            int[][] listOfQueens = actualGrid.getListOfQueens();
            // Zufällige Dame auswählen, indem Index zufällig generiert wird
            int randomQueen = rand.nextInt(listOfQueens.length);
            // Zufällige Dame auf dem Spielfeld speichern
            int[] actualRandomQueenPosition = listOfQueens[randomQueen];
            // Neue zufällige Position der gewählten Dame
            int[] newRandomQueenPosition = actualGrid.placeRandomPosition();

            // Spielfeld klonen, um vorherige Kosten mit nachherigen zu vergleichen (newGrid dient nur als Vergleichsspielfeld)
            newGrid = Utility.deepCopy(actualGrid);
            // Alte Dame auf dem Spielfeld löschen
            newGrid.deleteQueenAtIndex(actualRandomQueenPosition[0],actualRandomQueenPosition[1]);
            // Neue Dame setzen
            newGrid.setQueenAtIndex(newRandomQueenPosition[0],newRandomQueenPosition[1]);
            // Neue Queen in neuer Liste mit den anderen Damen speichern
            newGrid.changeQueensIndex(randomQueen, newRandomQueenPosition[0],newRandomQueenPosition[1]);

            int unvalidQueensSumNewGrid = Utility.unvalidQueensSum(newGrid);
            int unvalidQueensSumActualGrid = Utility.unvalidQueensSum(actualGrid);
            int unvalidQueensSumBestGrid = Utility.unvalidQueensSum(bestGrid);
            // Wenn neu berechneter "Wasserstand" besser/gleich ist als der aktuelle, Spielfeld updaten
            if (unvalidQueensSumNewGrid <= waterLevel){
                // Alte Dame in der Liste ersetzen
                listOfQueens[randomQueen] = newRandomQueenPosition;
                actualGrid.setQueenAtIndex(newRandomQueenPosition[0], newRandomQueenPosition[1]);
                actualGrid.deleteQueenAtIndex(actualRandomQueenPosition[0], actualRandomQueenPosition[1]);
                unvalidQueensSumActualGrid = Utility.unvalidQueensSum(actualGrid);
            }

            // Wenn Summe auf aktuellem Feld besser ist als auf dem bisherigen Besten, bestGrid updaten
             if (unvalidQueensSumActualGrid < unvalidQueensSumBestGrid){
                 bestGrid = Utility.deepCopy(actualGrid);
            }

            double newWaterLevel = waterLevel + growthRate;
            int unvalidQueensSumactualGrid = Utility.unvalidQueensSum(actualGrid);
            waterLevel = Math.max(newWaterLevel, unvalidQueensSumactualGrid);

            // Wenn Summe der falschen Damen zu oft vorkommt, Algorithmus beenden
            if (previousSum != -1000 && previousSum == Utility.unvalidQueensSum(actualGrid)){
                counter++;
            }
            previousSum = Utility.unvalidQueensSum(actualGrid);

            if (counter >= 200000){
                System.out.println("Du bist in einem lokalen Tiefpunkt :(");
                break;
            }
        }
        System.out.println("Iterationen: " + iteration);
    }

    public Spielfeld getBestGrid() {
        return bestGrid;
    }
}
