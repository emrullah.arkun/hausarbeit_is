import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

public class Spielfeld {
    private int[][] grid;
    private int[][] listOfQueens;

    public Spielfeld(int gridLength){
        grid = new int[gridLength][gridLength];
        listOfQueens = new int[gridLength][2];
        grid = buildGrid(gridLength);
    }

    public Spielfeld(int[][] grid, int[][] listOfQueensIndex){
        this.grid = grid;
        this.listOfQueens = listOfQueensIndex;
    }

    public void reset(int gridLength){
        grid = new int[gridLength][gridLength];
        listOfQueens = new int[gridLength][2];
    }
    public Spielfeld deepClone() {
        int gridLen = grid.length;
        int[][] newGrid = new int[gridLen][gridLen];
        int[][] newListOfQueensIndex = new int[gridLen][2];
        for (int i = 0; i < gridLen; i++) {
            newGrid[i] = new int[grid[i].length];
            for (int j = 0; j < grid[i].length; j++) {
                newGrid[i][j] = grid[i][j];
            }
        }

        for (int i = 0; i < listOfQueens.length; i++){
            newListOfQueensIndex[i] = new int[listOfQueens[i].length];
            for (int j = 0; j < listOfQueens[i].length; j++){
                newListOfQueensIndex[i][j] = listOfQueens[i][j];
            }
        }
        return new Spielfeld(newGrid, newListOfQueensIndex);
    }

    // Wählt eine zufällige Position für eine Dame aus
    public int[] placeRandomPosition(){
        // Zufällige neue Position
        SecureRandom rand = new SecureRandom();
        int gridLength = grid.length;
        int newX = rand.nextInt(gridLength);
        int newY = rand.nextInt(gridLength);
        while (queenIsSet(new int[]{newX,newY})){
            newX = rand.nextInt(gridLength);
            newY = rand.nextInt(gridLength);
        }
        return new int[]{newX,newY};
    }

    // Überprüft, ob eine Dame schon an der Stelle gesetzt ist
    public boolean queenIsSet(int[] index){
        for (int[] queen: listOfQueens){
            if (Arrays.equals(queen, index)) return true;
        }
        return false;
    }

    // Startzustand = Alle Damen zufällig auf dem Spielfeld verteilen
    public void randomPositions(){
        Random rand = new Random();
        int c = 0;
        int x;
        int y;
        while (c != grid.length){
            x = rand.nextInt(grid.length);
            y = rand.nextInt(grid.length);
            if (grid[x][y] != 1){
                grid[x][y] = 1;
                listOfQueens[c] = new int[]{x,y};
                c++;
            }
        }
    }
    public void changeQueensIndex(int x, int newX, int newY){
        listOfQueens[x] = new int[]{newX,newY};
    }
    public void setQueenAtIndex(int x, int y){
        grid[x][y] = 1;
    }

    public void deleteQueenAtIndex(int x, int y){
        grid[x][y] = 0;
    }
    public void setListOfQueens(int[][] listOfQueens) {
        this.listOfQueens = listOfQueens;
    }

    public void setGrid(int[][] grid) {
        this.grid = grid;
    }

    public int[][] getListOfQueens() {
        return listOfQueens;
    }

    public int[][] getGrid() {
        return grid;
    }

    // Baut ein grid und initialisiert alle Indizes mit 0
    public static int[][] buildGrid(int len){
        int[][] grid = new int[len][len];
        for (int i = 0; i < grid.length;i++){
            for (int j = 0; j < grid.length;j++){
                grid[i][j] = 0;
            }
        }
        return grid;
    }

    // Gibt das aktuelle Spielfeld aus
    public void printGrid(){
        for (int[] row: grid){
            System.out.println(Arrays.toString(row));
        }
    }
}
